const posts = require("./data");
const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors());

app.get("/api/posts", (req, res) => {
    res.status(200).json(posts);
});

app.get("/api/posts/:id", (req, res) => {
    const { id } = req.params;
    const post = posts.find(post => post.id === Number(id));
    if (post) return res.status(200).json(post);
    res.status(404).json({ message: "Post not found" });
});

const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
    console.log(`Server running on PORT=${PORT}`);
});