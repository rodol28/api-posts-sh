const posts = [
    {
        id: 1,
        title: "Departamento centrico",
        description: "Descripcion para el departamento centrico",
        address: "San Lorenzo 550, San Miguel de Tucumán",
        typeHouse: "Departamento",
        imageUrl: "https://previews.123rf.com/images/scanrail/scanrail1701/scanrail170100004/68678723-casa-de-construcci%C3%B3n-de-edificios-y-ciudad-concepto-la-noche-urbana-vista-exterior-de-las-modernas-casa.jpg",
        price: 17000,
        bedrooms: 1,
        bathrooms: 1,
        size: 40,
        rating: 4,
        reviews: 6,
        coordinates: {
            lat: "-26.82364102009025",
            lng: "-65.20149263914861"
        }
    },
    {
        id: 2,
        title: "Casa Rioja 500",
        description: "Descripcion para la casa en alquiler de la Rioja al 500",
        address: "Rioja 500, San Miguel de Tucumán",
        typeHouse: "Casa",
        imageUrl: "https://imgar.zonapropcdn.com/avisos/1/00/47/23/49/84/720x532/1760118335.jpg",
        price: 20000,
        bedrooms: 3,
        bathrooms: 1,
        size: 80,
        rating: 5,
        reviews: 5,
        coordinates: {
            lat: "-26.823430382875767",
            lng: "-65.20936760428368"
        }

    },
    {
        id: 3,
        title: "Casa en Cordoba 200",
        description: "Descripcion para la casa de la Cordoba al 200",
        typeHouse: "Departamento",
        address: "Cordoba 200, San Miguel de Tucumán",
        imageUrl: "https://imgar.zonapropcdn.com/avisos/1/00/46/69/78/03/1200x1200/1759934576.jpg",
        price: 23000,
        bedrooms: 4,
        bathrooms: 1,
        size: 90,
        rating: 4,
        reviews: 7,
        coordinates: {
            lat: "-26.827260089248465",
            lng: "-65.2095607233197"
        }
    },
    {
        id: 4,
        title: "Alquilo en Balcarce N° 1576",
        description: "Descripcion para la casa en alquiler en la Balcarse",
        typeHouse: "Casa",
        address: "Balcarce 1576, San Miguel de Tucumán",
        imageUrl: "https://static.tokkobroker.com/pictures/84892490041270548452994632638799258093736545774476091544092108971220211417182.jpg",
        price: 16000,
        bedrooms: 2,
        bathrooms: 1,
        size: 60,
        rating: 3,
        reviews: 2,
        coordinates: {
            lat: "-26.831141734184463",
            lng: "-65.2080795892106"
        }
    },
    {
        id: 5,
        title: "Dpto Laprida 200",
        description: "3 Dormitorios, Living, Comedor",
        typeHouse: "Departamento",
        address: "Laprida 200, San Miguel de Tucumán",
        imageUrl: "https://d2gue86ezsmq5i.cloudfront.net/eyJidWNrZXQiOiJyZXNlbS1hciIsImtleSI6IjgxNzcvLTEwODA3OTY4My5qcGciLCJlZGl0cyI6eyJyZXNpemUiOnsid2lkdGgiOjg0MCwiaGVpZ2h0Ijo2MzAsImZpdCI6ImNvdmVyIiwid2l0aG91dEVubGFyZ2VtZW50Ijp0cnVlfX19",
        price: 21000,
        bedrooms: 3,
        bathrooms: 1,
        size: 75,
        rating: 5,
        reviews: 1,
        coordinates: {
            lat: "-26.82279222700253",
            lng: "-65.20751032969625"
        }
    },
    {
        id: 6,
        title: "San Juan 1000",
        description: "Se trata de un departamento ubicado en calle San juan 1020 - planta alta - con acceso independiente desde la calle - Sin Expensas - y cuenta de: Comedor de diario: 4 x 4, Living comedor: 7.5 x 7.5 en L , Hall recepción: 4.5 x 2.2, Escritorio: 3.2 x 4.8, Asador: 2.8 x 6.2, Cocina comedor: 2.7 x 4 con despensa, Lavadero: 2.7 x 3.2, Dependencia de servicio con baño: 2.3 x 3.8, Toilette: 1.6 x 1.4, Baño principal: 2 x 3.2, Dormitorio 1: 3.3 x 3.4, Dormitorio 2: 3.5 x 2.8, Dormitorio 3: 3.3 x 4.5, Tiene un patio de luz y aire y un patio descubierto - Superficie Cubierta: 250 m2",
        typeHouse: "Departamento",
        address: "San Juan 1000, San Miguel de Tucumán",
        imageUrl: "https://static.tokkobroker.com/water_pics/112196351391543100323426283011931614918370314028468864117254041757755918295693.jpg",
        price: 35000,
        bedrooms: 4,
        bathrooms: 2,
        size: 100,
        rating: 5,
        reviews: 8,
        coordinates: {
            lat: "-26.824128709604327",
            lng: "-65.2101447069408"
        }
    },
];

module.exports = posts;